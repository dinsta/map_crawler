import json
import requests
import logging
import csv
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
# define environment
import os
os.environ["PROJ_LIB"] = "C:\ProgramData\Anaconda3\envs\BASEMAP\Library\share"
from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon

# enable logging messages
logging.basicConfig(level=logging.DEBUG)
logging.getLogger("requests").setLevel(logging.WARNING)
logging.getLogger("urllib3").setLevel(logging.WARNING)

base_url = 'https://web-finder.leafly.com/api/searchThisArea?'
session = requests.Session()
# enable retries
retry = Retry(connect=3, backoff_factor=0.5)
adapter = HTTPAdapter(max_retries=retry)
session.mount('http://', adapter)
session.mount('https://', adapter)

# define column headings
fieldnames = [
    'logoImage',
    'tagline',
    'city',
    'lastMenuUpdate',
    'medical',
    'address2',
    'tier',
    'customMapMarkerImage',
    'mapMarkerLevel',
    'phone',
    'numberOfReviews',
    'distanceKm',
    'sumRatingAllTime',
    'recreational',
    'slug',
    'distanceMi',
    'id',
    'name',
    'sumRating30Day',
    'featuredImageUrl',
    'hasReservationsEnabled',
    'starRating',
    'address1',
    'formattedShortLocation',
    'coverImage'
]


# split rectangle in 4 smaller rectangles
def get_split_positions(lat_tl, lng_tl, lat_br, lng_br):
    lat_tl = float(lat_tl)
    lng_tl = float(lng_tl)
    lat_br = float(lat_br)
    lng_br = float(lng_br)

    lng_dist = (lng_tl - lng_br) / 2
    lat_dist = (lat_tl - lat_br) / 2

    return [
        {"lat_tl": lat_tl - lat_dist, "lng_tl": lng_tl - lng_dist, "lat_br": lat_br, "lng_br": lng_br},
        {"lat_tl": lat_tl - lat_dist, "lng_tl": lng_tl, "lat_br": lat_br, "lng_br": lng_br + lng_dist},
        {"lat_tl": lat_tl, "lng_tl": lng_tl - lng_dist, "lat_br": lat_br + lat_dist, "lng_br": lng_br},
        {"lat_tl": lat_tl, "lng_tl": lng_tl, "lat_br": lat_br + lat_dist, "lng_br": lng_br + lng_dist},
    ]


# draw a rectangle on the map
def draw_rectangle(lats, lngs):
    m = Basemap(projection='cyl')
    m.drawmapboundary(fill_color='aqua')
    m.fillcontinents(color='coral', lake_color='aqua')
    m.drawcoastlines()

    x, y = m(lngs, lats)
    xy = zip(x, y)
    poly = Polygon(list(xy), facecolor='red', alpha=1)
    plt.gca().add_patch(poly)

    plt.show()


def get_records(lat_tl, lng_br, lat_br, lng_tl):
    # server can't return more than 200 records even if there are more in the specific rectangle
    max_results = 200
    url = base_url + "topLeftLat=" + str(lat_tl) + "&topLeftLon=" + str(lng_tl) + "&bottomRightLat=" + str(lat_br) + "&bottomRightLon=" + str(lng_br)
    # draw the current rectangle
    lats = [lat_br, lat_br, lat_tl, lat_tl]
    lngs = [lng_tl, lng_br, lng_br, lng_tl]
    draw_rectangle(lats, lngs)
    # get data from the current rectangle
    r = session.get(url)
    if r.status_code != 200:
        logging.error("error while fetching url %s" % url)
    # load data from GET response
    content = r.content.decode('utf8')
    json_data = json.loads(content)['dispensaries']
    logging.info("records %d" % len(json_data))
    """if server returned 200 records, split rectangle in 4 rectangles and
    call get_records function again for each split rectangle"""
    if len(json_data) >= max_results:
        logging.error("reached record limit splitting")
        split_positions = get_split_positions(lat_tl, lng_br, lat_br, lng_tl)

        for split_position in split_positions:
            split_json_data = get_records(split_position['lat_tl'], split_position['lng_br'], split_position['lat_br'], split_position['lng_tl'])
            # append records from all 4 rectangles
            for row in split_json_data:
                json_data.append(row)

        return json_data
    else:
        return json_data


def main():
    # define coordinate step size
    lat_inc = -10
    lng_inc = 20
    # open the CSV file
    output_file = open('leafly_data.csv', 'w', encoding='utf8', newline='')
    writer = csv.DictWriter(output_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL, fieldnames=fieldnames)
    writer.writeheader()

    records_found = {}
    records_written = 0
    # loop through the map
    for lat in range(80, -70, lat_inc):
        for long in range(-180, 180, lng_inc):
            # define the current rectangle
            lat_tr = lat
            lat_bl = lat + lat_inc
            lng_tr = long + lng_inc
            lng_bl = long
            # get records for the current rectangle
            json_data = get_records(lat_tr, lng_tr, lat_bl, lng_bl)

            for data_record in json_data:
                # check whether the found record is a duplicate
                if data_record['id'] in records_found:
                    logging.info("duplicate record %d" % data_record['id'])
                    continue
                records_found[data_record['id']] = True
                # write record in the CSV file
                record = {}
                for key in fieldnames:
                    if key in data_record:
                        record[key] = data_record[key]
                    else:
                        record[key] = ''
                writer.writerow(record)
                records_written += 1
                if records_written % 10 == 0:
                    print("records written %d" % records_written)

    output_file.close()


if __name__ == "__main__":
    main()
